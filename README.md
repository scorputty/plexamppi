# plexamppi

Personal project to store Raspbian configuration (automation) for setting up an Plexamp player.

### Roles
Each role will configure a complete Raspberry Pi for the DAC in the role name.
- example
```
plexamppi-topping-d10
```
Will do the configuration for the Topping D10 DAC.

## Notes;
More roles will follow when my hardware changes.
