#!/usr/bin/env bash

set_equalizer_curve() {
  curve="${*}"
  ctl=0
  for point in ${curve}
  do
    ctl=$(( ${ctl} + 1 ))
    echo cset numid=${ctl} ${point}
  done | amixer -D equal -s
}

profile="${1:-flat}"
case "${profile}" in
#  Freq     31 63 125 250 500 1 2  4  8  16
hd600) curve="66 70 65 62 63 62 65 63 63 66" ;;
flat) curve="65 65 65 65 65 65 65 65 65 65" ;;
*) echo "Unknown profile ${profile}" >&2 ;;
esac

[ "${curve}" ] && set_equalizer_curve "${curve}"
